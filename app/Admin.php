<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable; // must be added for authentication to work

class Admin extends Model implements \Illuminate\Contracts\Auth\Authenticatable
{
    //this is required for authentication to work
    use Authenticatable;
}
