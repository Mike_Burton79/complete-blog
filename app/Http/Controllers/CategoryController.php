<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Category;

// Required for the Ajax
use Illuminate\Support\Response;

class CategoryController extends Controller{

  public function getCategoryIndex()
  {
    $categories = Category::orderBY('created_at', 'desc')->paginate(2);
    return view('admin.blog.categories', ['categories' => $categories ]);
  } // getCategoryIndex



  public function postCreateCategory(Request $request)
  {
    $this->validate($request,[
      'name' => 'required|unique:categories'
    ]);

    $category = new Category();
    $category->name = $request['name'];
    if($category->save()){
      return Response::json(['message' => 'Category created'], 200);
    }
    return Response::json(['message' => 'Error during creation'], 404);
  }


  public function postUpdateCategory(Request $request)
  {
    $this->validate($request,[
      'name' => 'required|unique:categories'
    ]);

    $category = Category::find($request['category_id']);
      if(!$category){
        return Response::json(['message' => 'Category not found.'], 404);
      }
      $category->name = $request['name'];
      $category->update();
      return Response::json(['message' => 'Category update'], 200);
  }

}
