<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;
use App\Category;

class PostController extends Controller
{
    /*
    ** List all blog posts (frontend)
    */
    public function getBlogIndex()
    {
      $posts = Post::orderBy('created_at', 'desc')->paginate(3);
      // set excerpt
      foreach($posts as $post){
        $post->body = $this->shortenText( $post->body , 20 );
      }
      return view('frontend.blog.index', ['posts' => $posts]);
    }

    /*
    ** List all blog posts (backend)
    */
    public function getPostIndex()
    {
      $posts = Post::orderBy('created_at', 'desc')->paginate(3);

      return view('admin.blog.index', ['posts' => $posts]);
    }


    /*
    ** Post signle view
    */
    public function getSinglePost($post_id, $end = 'frontend')
    {
      // Fetch signle post
      $post = Post::find($post_id);
      if(!$post) {
        return redirect()->route('blog.index')->with(['fail' => 'Post not found!']);
      }

      return view( $end.'.blog.single', ['post' => $post ]);
    }

    // Backend posts controller deteails

    public function getCreatePost()
    {
      return view('admin.blog.create_post');
    }

    public function postCreatePost(Request $request)
    {
      $this->validate($request,[
        'title'  => 'required|max:120|unique:posts',
        'author' => 'required',
        'body'   => 'required'
      ]
    );

    // Save the post data
    $post         = new Post();
    $post->title  = $request['title'];
    $post->author = $request['author'];
    $post->body   = $request['body'];
    $post->save();

    // Logic to add categories

    return redirect()->route('admin.index')->with(['success' => 'Post successfully created!']);

    }

    /*
    ** Fetch update post view
    */
    public function getUpdatePost($post_id)
    {
      $post = Post::find($post_id);
      if(!$post){
        return redirect()->route('blog.index')->with(['fail' => 'Post not found!']);
      }

      return view('admin.blog.edit_post', ['post' => $post]);
    }

    /*
    ** Update the post
    */
    public function postUpdatePost(Request $request)
    {
      $this->validate($request,[
        'title'  => 'required|max:120', // remember to remove the unique:posts
        'author' => 'required',
        'body'   => 'required'
        ]
      );

      // Update the post data
      $post         = Post::find($request['post_id']);
      $post->title  = $request['title'];
      $post->author = $request['author'];
      $post->body   = $request['body'];
      $post->update();
      //categories

      return redirect()->route('admin.index')->with(['success' => 'Post successfully updated!']);

    }

    /*
    ** Delte post
    */
    public function getDeletePost($post_id)
    {
      $postDelete = Post::find($post_id);
      if(!$postDelete){
        return redirect()->route('blog.index')->with(['fail' => 'Post not found!']);
      }
      // delete post if it was found
      $postDelete->delete();

      return redirect()->route('admin.index')->with(['success' => 'Post deleted!']);
    }

    // Create the excerpt function
    private function shortenText( $text , $words_count )
    {
      if(str_word_count( $text , 0 ) > $words_count ){
        $words = str_word_count( $text, 2 );
        $pos = array_keys( $words );
        $text = substr($text, 0 , $pos[$words_count]).'...';
      }
      return $text;
    }
}
