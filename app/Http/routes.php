<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', [
      'uses' => 'PostController@getBlogIndex',
      'as'   => 'blog.index'
    ]);
    Route::get('/blog', [
      'uses' => 'PostController@getBlogIndex',
      'as'   => 'blog.index'
    ]);

    Route::get('/single/{post_id}&{end}', [
      'uses' => 'PostController@getSinglePost',
      'as'   => 'blog.single'
    ]);

    /* Page routes */
    Route::get('/about', function() {
      return view('frontend.pages.about');
    })->name('about');

    Route::get('/contact', function() {
      return view('frontend.pages.contact');
    })->name('contact');

    //route group for the admin prefixed with Admin
    Route::group([
      'prefix' => '/admin'
    ], function(){

      Route::get('/',[
        'uses' => 'AdminController@getIndex',
        'as'   => 'admin.index'
      ]);

      Route::get('/blog/posts', [
        'uses' => 'PostController@getPostIndex',
        'as'   => 'admin.blog.index'
      ]);

      /*
      ** Backend single blog post
      */
      Route::get('/blog/post/{post_id}&{end}', [
        'uses' => 'PostController@getSinglePost',
        'as'   => 'admin.blog.post'
      ]);


      /*
      ** Create Posts area backend
      */

      Route::get('/blog.posts/create', [
        'uses' => 'PostController@getCreatePost',
        'as'   => 'admin.blog.create_post'
      ]);

      Route::post('/blog/post/create', [
        'uses' => 'PostController@postCreatePost',
        'as'   => 'admin.blog.post.create'
      ]);

      Route::post('/blog/category/create', [
        'uses' => 'CategoryController@postCreateCategory',
        'as'   => 'admin.blog.category.create'
      ]);

      Route::post('/blog/categories/update',[
        'uses' => 'CategoryController@postUpdateCategory',
        'as' => 'admin.blog.category.update'
      ]);

      /*
      **  Get the update form view
      */
      Route::get('/blog/post/{post_id}/edit', [
        'uses' => 'PostController@getUpdatePost',
        'as'   => 'admin.blog.post.edit'
      ]);

      /*
      ** Submit the update
      */
      Route::post('/blog/post/update', [
        'uses' => 'PostController@postUpdatePost',
        'as'   => 'admin.blog.post.update'
      ]);

      /*
      ** Delete post
      */
      Route::get('/blog/post/delete/{post_id}', [
        'uses' => 'PostController@getDeletePost',
        'as' => 'admin.blog.post.delete'
      ]);

      /*
      ** Categories
      */
      Route::get('/blog/categories',[
        'uses' => 'CategoryController@getCategoryIndex',
        'as'   => 'admin.blog.categories'
      ]);



    }); // end of admin route group

});
