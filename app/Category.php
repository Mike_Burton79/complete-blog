<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    // Create relation between post and Category
    public function posts()
    {
      return $this->hasMany('App\Post', 'posts_categories');
    }
}
