@extends('layouts.master')

@section('browser-title')
  Blog index
@endsection

@section('content')
@include('includes.info-box')
@if(count($posts) == 0)
  <h3>There are no posts to show</h3>
@else
  @foreach($posts as $post)
    <article class="blog-post">
      <h3>{{ $post->title }}</h3>
      <span class="subtitle">{{ $post->author }} | {{ $post->created_at }}</span>
      <p>
        {{ $post->body }}
      </p>
      <a href="{{ route('blog.single', ['post_id' => $post->id, 'end' => 'frontend'])}}" class="btn btn-primary">Read more...</a>
    </article>
 @endforeach
@endif


<section class="pagination">
  {!! $posts->links() !!}
</section>
@endsection
