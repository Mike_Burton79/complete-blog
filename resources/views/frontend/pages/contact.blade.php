@extends('layouts.master')


@section('browser-title')
  Contact
@endsection

@section('styles')

@endsection

@section('content')
  <h1>Contact</h1>

  @include('includes.info-box')

  <form class="contact-form" action="" method="post">
    <div class="form-group">
      <label for="name">Name:</label>
      <input type="text" name="name" value="" class="form-control">
    </div>

    <div class="form-group">
      <label for="email">Email</label>
      <input type="text" name="email" value="" class="form-control">
    </div>

    <div class="form-group">
      <label for="subject">Subject:</label>
      <input type="text" name="subject" value="" class="form-control">
    </div>

    <div class="form-group">
      <label for="message">Message:</label>
      <textarea name="message" rows="10" class="form-control" ></textarea>
    </div>

    <button type="submit" name="button" class="btn btn-primary">Submit</button>
    <input type="hidden" name="_token" value="{{ Session::token() }}">
  </form>

@endsection
