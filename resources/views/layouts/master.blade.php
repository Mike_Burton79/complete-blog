<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>@yield('browser-title')</title>

  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{URL::to('src/css/style.css')}}" media="all" charset="utf-8">
  @yield('styles')
</head>
<body>
  @include('includes.header')
    <div class="container main" style="padding-top: 70px;">
        @yield('content')
    </div>
  @include('includes.footer')
</body>
</html>
