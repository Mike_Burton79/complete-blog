<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Admin area</title>

  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{URL::to('src/css/admin.css')}}" media="all" charset="utf-8">
  @yield('styles')
</head>
<body>
  @include('includes.admin-header')

  @yield('content')

  <script type="text/javascript">
    var baseUrl = "{{ URL::to('/') }}"; // same as site_url() in wordpress
  </script>

  @yield('scripts')
</body>
</html>
