@extends('layouts.admin-master')

@section('styles')
  <link rel="stylesheet" href="{{ URL::to('src/css/form.css') }}" type="text/css">
@endsection

@section('content')
  <div class="container">
    @include('includes.info-box')

    <form  action="{{ route('admin.blog.post.create') }}" method="post">

      <div class="input-group">
        <label for="title">Title</label>
        <input type="text" name="title" value="{{ Request::old('title') }}" {{ $errors->has('title') ? 'class=has-error' : '' }}>
      </div>

      <div class="input-group">
        <label for="author">Author</label>
        <input type="text" name="author" value="{{ Request::old('author') }}" {{ $errors->has('author') ? 'class=has-error' : '' }}>
      </div>

      <div class="input-group">
        <label for="category_select">Add Categories</label>
        <select id="category_select" name="category_select">
          <option value="Dummy category id">Dummy category</option>
        </select>
        <button type="button" class="btn"> Add Category</button>

        <div class="added-categories">
          <ul>

          </ul>
        </div>
        <!-- hidden categories to be submitted with the form -->
        <input type="hidden" name="categories" value="" id="categories">
      </div>

      <div class="input-group">
        <label for="body">Body</label>
        <textarea name="body" rows="8" cols="40" id="body" {{ $errors->has('body') ? 'class=has-error' : '' }}>{{ Request::old('body') }}</textarea>
      </div>

      <button type="submit" class="btn">Create Post</button>
      <input type="hidden" name="_token" value="{{ Session::token() }}">

    </form>
  </div>
@endsection

@section('scripts')
  <script src="{{ URL::to('src/js/posts.js') }}"></script>

@endsection
